\section{Overview}
\label{sec:overview}

As \autoref{fig:overview} shows, the implementation can be divided in four
separate blocks: \nameref{subsec:appUI}, the skeleton of our \emph{frontend};
\nameref{subsec:app} the functionalities of our \emph{frontend} that connect it
to the \emph{backend}; \nameref{subsec:AV_input} the main part of the
\emph{backend} that interacts with the underlying video file; Finally
\nameref{subsec:processors} which form part of our \emph{backend} and that
takes care to prepare video and audio data to be displayed.

Development and testing were carried out using Ubuntu 20.04 in
\href{https://docs.microsoft.com/en-us/windows/wsl/about}{Windows Subsystem for
Linux} using \href{https://code.visualstudio.com/}{Visual Studio Code} as
Integrated Development Environment. All source code is available in the
\href{\repo}{code repository}.

\begin{figure}
    \centering
    \includegraphics[width=.45\textwidth]{\subdir/overview.drawio.png}
    \caption{Implementation structure}
    \label{fig:overview}
\end{figure}

\subsection{User Interface}
\label{subsec:appUI}

The User Interface is built using FLTK \cite{FLTK} library. We used the Fast
Light User Interface Designer
(\href{https://www.fltk.org/doc-1.3/fluid.html}{FLUID}) in order to graphically
design our UI and generate the associated C++ code: \reposrc{appUI}.
\autoref{fig:appUI-tree} shows the tree of widgets that compose the UI in FLUID.

\begin{figure}
    \centering
    \includegraphics[width=.45\textwidth]{\subdir/appUI-tree.jpg}
    \caption{Components tree of the User Interface built with FLUID}
    \label{fig:appUI-tree}
\end{figure}

\subsection{Application}
\label{subsec:app}

\autoref{fig:app} illustrates the contents of \reposrc{app}, which contains the
callbacks for the widgets of the User Interface and some state information.
These callbacks interact with the \nameref{subsec:AV_input} in order to load a
file, get images to be displayed and move to a certain position. Moreover, it
takes care of displaying the images at the appropriate time, which is defined
by the file frame rate and the selected playback speed. This displaying of
images is done asynchronously using a FLTK
\href{https://www.fltk.org/doc-1.3/classFl.html#a23e63eb7cec3a27fa360e66c6e2b2e52}{timeout
handler} that consumes processed audio and video images from the
\nameref{subsec:AV_input}.

\begin{figure}
    \centering
    \includegraphics[width=.45\textwidth]{\subdir/app.drawio.png}
    \caption{Simplified structure of the interaction between the User Interface and the audiovisual input}
    \label{fig:app}
\end{figure}

\subsection{Audiovisual input}
\label{subsec:AV_input}

This is probably the most interesting part of the project as it is the part
that benefits the most from being written in C++. We have full control over the
flow of data from the input and its processing as it is extracted. However,
\emph{with great power comes great responsibility} as we need to define exactly
how will the data be consumed and how to move to other parts of the file
cleaning up the data that has been processed but not displayed. There are
different ways of solving this problem, each with its own advantages, in this
work we have opted to minimize the amount of data that is stored in memory,
recreating a situation where the data is read in real time from a device
instead of a file.

\autoref{fig:AV_input} illustrates how audio and video images are generated
from an encoded file, so they can be displayed in the User Interface as it has
been explained in \autoref{subsec:app}.

\begin{figure}
    \centering
    \includegraphics[width=.45\textwidth]{\subdir/AV_input.drawio.png}
    \caption{Application structure}
    \label{fig:AV_input}
\end{figure}

By \emph{loading} a video we initialize the required libraries taking into
account the file format. Then, we use three threads to process the audiovisual
input in a \emph{producer/consumer} scenario. The threads share data using four
different \emph{queues}:
\begin{itemize}
    \item \texttt{video.frames}: Decoded images from the video stream.
    \item \texttt{audio.frames}: Decoded audio frames from the audio stream.
    \item \texttt{video.images}: Processed images from the video stream.
    \item \texttt{audio.images}: Processed audio frames.
\end{itemize}

This \emph{queues} have been extended from the standard
\href{https://en.cppreference.com/w/cpp/container/queue}{\texttt{std::queue}}
in order to be thread safe. We use
\href{https://en.cppreference.com/w/cpp/thread/mutex}{\texttt{std::mutex}} in
order to protect the data from being simultaneously accessed by the threads.
We also use
\href{https://en.cppreference.com/w/cpp/thread/condition_variable}{\texttt{std::condition\_variable}}
to synchronize the threads: blocking a consumer from extracting a data from a
queue while it is empty and awaking it when a producer has added new data to
that queue; blocking a producer from adding a data to a queue while it is full
and awaking it when a consumer has extracted a data from that queue.

\paragraph{Read}
We use \libavformat in order to \emph{demuxe} the input file, divide it in
packets of audio and video data. Audio packets are decoded with \libavcodec
into \href{https://ffmpeg.org/doxygen/trunk/structAVFrame.html}{AVFrames}.
Video packets are instead decoded with \OpenCV into
\href{https://docs.opencv.org/4.x/d3/d63/classcv_1_1Mat.html}{OpenCV matrices}.
Therefore this is a \emph{producer} for \texttt{audio.frames} and
\texttt{video.frames}.

\paragraph{Process Video}
\emph{Consumer} of \texttt{video.frames} that uses the selected video processor
to \emph{produce} an image and store it in \texttt{video.images}.

\paragraph{Process Audio}
\emph{Consumer} of \texttt{audio.frames}. Unfortunately, an audio frame does
not need to correspond to a video frame in duration. Due to that, this thread
accumulates the audio data from several frames according to their timestamps,
synchronizing them with the video images timing. Then it uses the selected
audio processor to \emph{produce} an image from the accumulated audio data and
store it in \texttt{audio.images}.

In order to have the ability to move backwards in the file, we need free the
queues from the buffered data. Freeing the data shared between the threads
without incurring in a deadlock is not a trivial task and we have spent a
significant amount of development time debugging this issue. The details of the
implementation can be found in \reposrc{AV\_input}.

\subsection{Processors}
\label{subsec:processors}

Processors are designed to be the part of the project that is easy to extend.
They are static functions that take as input video or audio data and return the
image that should be displayed. Processor functions are connected to the
\nameref{subsec:AV_input} in a way that the processing threads of the
audiovisual input call the corresponding processor function. 

Processor functions do not have access to the audiovisual input, but can be
initialized by it when a new file is loaded in the case that the processing
function depends on any parameter of the audiovisual input apart from the data
itself. For example the processor that computes Mel-frequency cepstrum
coefficients from the audio data depends on the audio sampling rate. Therefore
we have implemented a \emph{factory} function that creates the processor
function when the audiovisual input is loading a new file.